mod config;
use dwm_statusbar_channels::start_bar;

start_bar!(config::get_config());
